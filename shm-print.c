#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "structs.h"

int main(void) {
  //Get shared memory
  int shm = shmget(shmid, sizeof(st), IPC_CREAT | 0660);
  printf("shmget %d\n", shm);

  st* a;
  a = (st*)shmat(shm, NULL, SHM_RND);

  for(;;) {
    printf("shmread a %d\n", a->a);
    printf("shmread b %d\n", a->b);
    sleep(1);
  }
}
