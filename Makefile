all: shm-print shm-write

shm-print:
	gcc shm-print.c -o shm-print

shm-write:
	gcc shm-write.c -o shm-write

clean:
	rm shm-write
	rm shm-print
