import sysv_ipc
import struct

#fmt = 'BBB'

fmt = 'Q'

# Create shared memory object
memory = sysv_ipc.SharedMemory(4242)

# Read value from shared memory
memory_value = memory.read()
a = struct.pack(fmt, 653)
b = struct.pack(fmt, 652)

# Find the 'end' of the string and strip
i = struct.unpack_from(fmt, memory_value)
print(i[0])

memory.write(a, 0)
memory.write(b, 8)
