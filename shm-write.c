#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include "structs.h"

int main(int argc, char** argv) {
  //Get shared memory
  int shm = shmget(shmid, sizeof(st), IPC_CREAT | 0660);
  printf("shmget %d\n", shm);

  st* a;
  a = (st*)shmat(shm, NULL, SHM_RND);
  printf("errno %d\n", errno);
  printf("addr %x\n", a);

  uint64_t i = 0;
  for(;;) {
    printf("shmval %d\n",i);
    a->a = i;
    i++;
    sleep(1);
  }

  shmdt(shmid);
  shmctl(shmid, IPC_RMID, NULL);
}
